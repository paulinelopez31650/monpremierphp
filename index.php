<!DOCTYPE html>
<html>
    <head>
        <title>Notre première instruction : echo</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css">
    </head>
    <body>    
        <section class="ex1">    
            <!-- Faire un formulaire pour récupérer les données saisies -->
            <form action="cible.php" method="post"> 
            <?php echo "Exercice 1 : Comment tu t'appelle?"; ?>
            </br>
                <input type="text"  minlength="1" maxlength="10" placeholder="entre 1 et 10 caractéres" name="in">
                <input type="submit" value="Valider" /> 
            </br>
            </form>
            </br>
                </br>
        </section>
        <section class="ex2">
            <!-- Faire une identifiant et un mot de passe -->
            <form action="secret.php" method="POST">
            <?php echo "Exercice 2: identifiant & mot de passe"; ?>
            <input type="text" name="email" placeholder="@">
            <label for="pass">Mot de passe (8 caracteres minimum):</label>
            <input type="password" name="password" minlength="8" required>
            <input type="submit" value="Valider" /> 
            </form>
            
            </br>
            </br>
        </section>
        <section class="ex3">
            <!-- Calculer une moyenne avec 10 notes -->
            <form action="secret.php" method="post"> 
            <?php echo "Exercice 3: Calculer une moyenne avec 10 notes "; ?>
            </br>
            <input class="notes" type="number"  minlength="1" maxlength="2" placeholder="" id="note1">
            <input class="notes" type="number"  minlength="1" maxlength="2" placeholder="" id="note2">
            <input class="notes" type="number"  minlength="1" maxlength="2" placeholder="" id="note3">
            <input class="notes" type="number"  minlength="1" maxlength="2" placeholder="" id="note4">
            <input class="notes" type="number"  minlength="1" maxlength="2" placeholder="" id="note5">
            <input class="notes" type="number"  minlength="1" maxlength="2" placeholder="" id="note6">
            <input class="notes" type="number"  minlength="1" maxlength="2" placeholder="" id="note7">
            <input class="notes" type="number"  minlength="1" maxlength="2" placeholder="" id="note8">
            <input class="notes" type="number"  minlength="1" maxlength="2" placeholder="" id="note9">
            <input class="notes" type="number"  minlength="1" maxlength="2" placeholder="" id="note10">


    <button type="button" onclick="afficherMoyenne()">Clique pour calculer</button>

        </section>
        </form>

    </body>
</html>